package com.example.spinnerdropdownlist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Spinner spn1, spn2;
    private Button btn1;
    private ArrayList<String> itemlist = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spn1 = (Spinner) findViewById(R.id.spinner1);
        spn2 = (Spinner) findViewById(R.id.spinner2);
        btn1 = (Button) findViewById(R.id.button1);
        final String[] arrCountry =
                getResources().getStringArray(R.array.country_array);
        ArrayAdapter<String> arrAdt1 = new
                ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, arrCountry);
        spn1.setAdapter(arrAdt1);
        itemlist.add("data 1");
        itemlist.add("data 2");
        itemlist.add("data 3");
        itemlist.add("data 4");
        //ArrayAdapter arrAdt2 = new
        new ArrayAdapter(this,android.R.layout.simple_spinner_item);
        ArrayAdapter<String> arrAdt2 = new
                ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, itemlist);
        spn2.setAdapter(arrAdt2);
        spn1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int
                    position, long id) {
                Toast.makeText(getApplicationContext(), "Select : " +
                                parent.getItemAtPosition(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spn2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int
                    position, long id) {
                Toast.makeText(getApplicationContext(), "Select : " +
                                parent.getItemAtPosition(position).toString(),
                        Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Result : " +
                                "\nSpinner 1 : " +
                                String.valueOf(spn1.getSelectedItem()) +
                                "\nSpinner 2 : " +
                                String.valueOf(spn2.getSelectedItem()),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}